import React, { useState, useEffect } from 'react';
import Manufacturer from './Manufacturer';

function Manufacturers() {
    const [manufacturers, setManufacturers] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    const deleteManufacturer = async (id) => {
        const url = `http://localhost:8100/api/manufacturers/${id}`;
        const fetchConfig = {
            method: 'DELETE',
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setManufacturers(manufacturers.filter(manufacturer => manufacturer.id !== id));
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Manufacturer</th>
            </tr>
            </thead>
            <tbody>
                {manufacturers.map((manufacturer) => {
                    return (
                        <Manufacturer key={manufacturer.id} manufacturer={manufacturer} deleteManufacturer={deleteManufacturer}/>
                    );
                })}
            </tbody>
      </table>
    )
}

export default Manufacturers;
