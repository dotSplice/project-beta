import React, { useState, useEffect } from 'react';

function ServiceHistory() {
    const [vin, setVin] = useState('');
    const [appointments, setAppointments] = useState([]);
    const [filteredAppointments, setFilteredAppointments] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAppointments(data);
            setFilteredAppointments(data)
        }
    }

    const formatDate = (datetime) => {
        const date = new Date(datetime);
        const month = date.getMonth() + 1;
        const day = date.getDate();
        const year = date.getFullYear();
        return `${month}/${day}/${year}`;
    };

    const formatTime = (datetime) => {
        const date = new Date(datetime);
        let hours = date.getHours();
        let minutes = date.getMinutes();
        const ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12 || 12;
        minutes = minutes < 10 ? `0${minutes}` : minutes;
        return `${hours}:${minutes} ${ampm}`;
    };

    useEffect(() => {
        fetchData();
    }, []);
    const handleSubmit = (e) => {
        e.preventDefault();
        const filteredAppointmentList = appointments.filter(appointment => appointment.vin === vin);
        setFilteredAppointments(filteredAppointmentList);

    }
    return (
        <div>
            <h1>Service History</h1>
            <form onSubmit={handleSubmit} id="search-vin">
                <div className="input-group mb-3">
                    <input className="form-control" required type="text" name="vin" id="vin" placeholder="Search by VIN..." value={vin} onChange={(e)=> setVin(e.target.value)} />
                    <button className="btn btn-outline-secondary" type="submit" id="button-addon2">Search</button>
                </div>
            </form>
            <table className="table table-striped">
            <thead>
            <tr>
                <th>VIN</th>
                <th>Is VIP?</th>
                <th>Customer</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
                {filteredAppointments.map((appointment) => {
                    return (
                        <tr key={appointment.id}>
                            <td>{ appointment.vin }</td>
                            <td>{ appointment.vip ? 'Yes' : 'No' }</td>
                            <td>{ appointment.customer }</td>
                            <td>{ formatDate(appointment.date_time) }</td>
                            <td>{ formatTime(appointment.date_time) }</td>
                            <td>{ appointment.technician.first_name } {appointment.technician.last_name}</td>
                            <td>{ appointment.reason }</td>
                            <td>{ appointment.status }</td>
                        </tr>
                    );
                })}
            </tbody>
            </table>
        </div>
    );
}

export default ServiceHistory;
