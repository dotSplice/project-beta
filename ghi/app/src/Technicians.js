import React, { useState, useEffect } from 'react';
import Technician from './Technician';

function Technicians() {
    const [technicians, setTechnicians] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data);
        }
    }

    const deleteTechnician = async (id) => {
        const url = `http://localhost:8080/api/technicians/${id}`;
        const fetchConfig = {
            method: 'DELETE',
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setTechnicians(technicians.filter(technician => technician.id !== id));
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Employee ID</th>
            </tr>
            </thead>
            <tbody>
                {technicians.map((technician) => {
                    return (
                        <Technician key={technician.id} technician={technician} deleteTechnician={deleteTechnician}/>
                    );
                })}
            </tbody>
      </table>
    );
}

export default Technicians;
