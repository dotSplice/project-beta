import React, { useState, useEffect } from 'react';
import Appointment from './Appointment';

function Appointments() {
    const [appointments, setAppointments] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8080/api/appointments/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAppointments(data);
        }

    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <table className="table table-striped">
            <thead>
            <tr>
                <th>Customer</th>
                <th>isVIP?</th>
                <th>Date</th>
                <th>Time</th>
                <th>Technician</th>
                <th>Reason</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody>
            {appointments.map((appointment) => {
                return (
                    <Appointment key={appointment.id} appointment={appointment} />
                );
            })}
            </tbody>
      </table>
    );
}

export default Appointments;
