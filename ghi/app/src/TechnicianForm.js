import React, { useState } from 'react';

function TechnicianForm() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeId;

        const url = 'http://localhost:8080/api/technicians/';
        const fetchConfig = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            // const newTechnician = await response.json();
            setFirstName('');
            setLastName('');
            setEmployeeId('');
        }
    }


    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a Technician</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input onChange={(e)=> setFirstName(e.target.value)} placeholder="First Name" required type="text" name="first_name" id="first_name" className="form-control" value={firstName}/>
                            <label htmlFor="first_name">First Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e)=> setLastName(e.target.value)} placeholder="Last Name" required type="text" name="last_name" id="last_name" className="form-control" value={lastName}/>
                            <label htmlFor="last_name">Last Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e) => setEmployeeId(e.target.value )} placeholder="Employee ID" required type="text" name="employee_id" id="employee_id" className="form-control" value={employeeId}/>
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Add a Technician</button>
                    </form>
            </div>
        </div>
        </div>
    )
}

export default TechnicianForm
