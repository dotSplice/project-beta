import React, {useState} from 'react';

export default function CustomerForm() {

    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [phone_number, setPhoneNumber] = useState('');
    const [address, setAddress] = useState('');


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {
            "first_name": firstName,
            "last_name": lastName,
            "phone_number": phone_number,
            "address": address
        }
        const url = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data),
        }
        const response = await fetch(url, fetchConfig)
        if (response.ok) {
            setFirstName('');
            setLastName('');
            setPhoneNumber('');
            setAddress('');
        }
    }

    return (
        <div className='container mx-auto mt-5 p-4 bg-light rounded'>
            <h1 className=' text-center mb-4 fw-bold '>Add a Customer</h1>
            <form onSubmit={handleSubmit}>
                <div className="mb-3">
                    <label htmlFor="name" className="form-label">First Name</label>
                    <input type="text" className="form-control" id="name" value={firstName} onChange={(event) => setFirstName(event.target.value)} />
                </div>
                <div className="mb-3">
                    <label htmlFor="lastName" className="form-label">Last Name</label>
                    <input type="lastName" className="form-control" id="lastName" value={lastName} onChange={(event) => setLastName(event.target.value)} />
                </div>
                <div className="mb-3">
                    <label htmlFor="phone" className="form-label">Phone Number</label>
                    <input type="text" className="form-control" id="phone_number" value={phone_number} onChange={(event) => setPhoneNumber(event.target.value)} />
                </div>
                <div className="mb-3">
                    <label htmlFor="address" className="form-label">Address</label>
                    <input type="text" className="form-control" id="address" value={address} onChange={(event) => setAddress(event.target.value)} />
                </div>
                <button type="submit" className="btn btn-primary">Submit</button>
            </form>
        </div>
    )
}
