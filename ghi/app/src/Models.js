import React, { useState, useEffect } from 'react';
import Model from './Model';

function Models(){
    const [models, setModels] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';

        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }
    }

    const deleteModel = async (id) => {
        const url = `http://localhost:8100/api/models/${id}`;
        const fetchConfig = {
            method: 'DELETE',
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setModels(models.filter(model => model.id !== id));
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return(
        <table className="table table-striped">
        <thead>
        <tr>
            <th>Name</th>
            <th>Manufacturer</th>
            <th>Picture</th>
        </tr>
        </thead>
        <tbody>
            {models.map((model) => {
                return (
                    <Model key={model.id} model={model} deleteModel={deleteModel}/>
                );
            })}
        </tbody>
  </table>
    )
}

export default Models
