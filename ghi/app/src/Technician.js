import React from 'react';

function Technician(props) {
    return (
        <tr key={props.technician.id}>
            <td>{ props.technician.first_name }</td>
            <td>{ props.technician.last_name }</td>
            <td>{ props.technician.employee_id }</td>
            <td><button className="btn btn-danger" onClick={() => props.deleteTechnician(props.technician.id)}>Delete</button></td>
      </tr>
    );
}

export default Technician;
