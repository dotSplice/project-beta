import React, { useState, useEffect } from 'react';
import Salesperson from './Salesperson';


export default function SalespersonHistory() {
    const [salespersonHistory, setSalespersonHistory] = useState([]);
    const [salesperson, setSalesperson] = useState('');

    const getSalespersonHistory = async () => {
        const url = 'http://localhost:8090/api/sales/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            const dataWithVin = data.map((sale) => {
                return {
                    ...sale,
                    automobile: {
                      ...sale.automobile,
                      vin: sale.automobile.import_href.slice(17, 34)
                    }
                }
            })
            setSalespersonHistory(dataWithVin);
        }
    }
    const filterBySalesperson = (event, id) => {
        event.preventDefault();
        const selectedSalespersonId = id;
        setSalesperson(selectedSalespersonId);
        const filteredHistory = salespersonHistory.filter(history => history.salesperson.id === Number(selectedSalespersonId));
        setSalespersonHistory(filteredHistory);

    }


    useEffect(() => {
        getSalespersonHistory();

    }, []);

    return (
        <div className="container">
            <h1 className="text-center mb-4 fw-bold" >Salesperson History</h1>

            <select className="form-select" aria-label="Default select example" id="salesperson" value={salesperson} onChange={(event) => filterBySalesperson(event, event.target.value)} >
                <option value="" >Select Salesperson</option>
                {salespersonHistory.map((salesperson) => (
                    <option key={salesperson.id} value={salesperson.salesperson.id}>{salesperson.salesperson.first_name} {salesperson.salesperson.last_name}</option>
                ))}
            </select>

            <table className="table">
                <thead>
                    <tr>
                        <th>Salesperson Name</th>
                        <th>Customer Name</th>
                        <th>VIN</th>
                        <th>Price</th>
                    </tr>
                </thead>

                <tbody>
                    {salespersonHistory.map((sale) => {
                        return (
                            <tr key={sale.id}>
                                <td>{sale.salesperson.first_name} {sale.salesperson.last_name}</td>
                                <td>{sale.customer.first_name} {sale.customer.last_name}</td>
                                <td>{sale.automobile.vin}</td>
                                <td>{sale.price}</td>
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    )
}
