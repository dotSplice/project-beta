import React from "react";


export default function Salesperson(props) {
    return (
        <tr key={props.salesperson.id}>
            <td>{props.salesperson.first_name}</td>
            <td>{props.salesperson.last_name}</td>
            <td>{props.salesperson.employee_id}</td>
            <td><button className="btn btn-danger" onClick={() => props.deleteSalesperson(props.salesperson.id)}>Delete</button></td>
        </tr>
    )
}
