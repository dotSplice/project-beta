import React from "react";


export default function Sale(props) {
    return (
        <tr key={props.sale.id}>
            <td>{props.sale.salesperson.employee_id}</td>
            <td>{props.sale.salesperson.first_name} {props.sale.salesperson.last_name}</td>
            <td>{props.sale.customer.first_name} {props.sale.customer.last_name}</td>
            <td>{props.sale.automobile.vin}</td>
            <td>{props.sale.price}</td>
            <td><button className="btn btn-danger" onClick={() => props.deleteSale(props.sale.id)}>Delete</button></td>
        </tr>
    )
}
