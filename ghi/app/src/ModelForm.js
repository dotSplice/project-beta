import React, { useState, useEffect } from 'react';

function ModelForm() {
    const [manufacturers, setManufacturers] = useState([]);
    const [name, setName] = useState('');
    const [manufacturer, setManufacturer] = useState('');
    const [pictureUrl, setPictureUrl] = useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/manufacturers/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setManufacturers(data.manufacturers);
        }
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {
            name,
            manufacturer_id: Number(manufacturer),
            picture_url: pictureUrl
        }
        const url = 'http://localhost:8100/api/models/';
        const fetchConfig = {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(data)
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            // const newModel = await response.json();
            setManufacturer('');
            setName('');
            setPictureUrl('');

        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a Model</h1>
                    <form onSubmit={handleSubmit} id="add-model-form">
                        <div className="form-floating mb-3">
                            <input onChange={(e)=> setName(e.target.value)} placeholder="Automobile VIN" required type="text" name="name" id="name" className="form-control" value={name}/>
                            <label htmlFor="name">Model Name</label>
                        </div>
                        <div className="mb-3">
                                <select onChange={(e)=> setManufacturer(e.target.value)} required name="manufacturer" id="manufacturer" className="form-select" value={manufacturer}>
                                <option value="">Select a Manufacturer</option>
                                {manufacturers.map(manufacturer => {
                                    return (
                                        <option key={manufacturer.id} value={manufacturer.id}>{manufacturer.name}</option>
                                    )
                                })}
                                </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e)=> setPictureUrl(e.target.value)} placeholder="Picture URL" required type="text" name="picture_url" id="picture_url" className="form-control" value={pictureUrl}/>
                            <label htmlFor="picture_url">Picture URL</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Add Model</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ModelForm
