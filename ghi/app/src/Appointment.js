import React, { useState, useEffect } from 'react';

function Appointment(props) {
    const [status, setStatus] = useState(props.appointment.status);

    const formatDate = (datetime) => {
        const date = new Date(datetime);
        const month = date.getMonth() + 1;
        const day = date.getDate();
        const year = date.getFullYear();
        return `${month}/${day}/${year}`;
    };

    // Function to format the time as hh:mm am/pm
    const formatTime = (datetime) => {
        const date = new Date(datetime);
        let hours = date.getHours();
        let minutes = date.getMinutes();
        const ampm = hours >= 12 ? 'pm' : 'am';
        hours = hours % 12 || 12;
        minutes = minutes < 10 ? `0${minutes}` : minutes;
        return `${hours}:${minutes} ${ampm}`;
    };

    const cancelAppointment = async (e, id) => {
        e.preventDefault();
        // console.log(id)
        const url = `http://localhost:8080/api/appointments/${id}/cancel`;
        const fetchConfig = {
            method: 'PUT',
        }
        const response = await fetch(url, fetchConfig);
        setStatus('canceled');


    }

    const FinishAppointment = async (e, id) => {
        e.preventDefault();
        // console.log(id)
        const url = `http://localhost:8080/api/appointments/${id}/finished`;
        const fetchConfig = {
            method: 'PUT',
        }
        const response = await fetch(url, fetchConfig);
        setStatus('finished');
    }


    return (
        <tr key={props.appointment.id}>
            <td>{ props.appointment.customer }</td>
            <td>{ props.appointment.vip ? 'Yes' : 'No' }</td>
            <td>{ formatDate(props.appointment.date_time) }</td>
            <td>{ formatTime(props.appointment.date_time) }</td>
            <td>{ props.appointment.technician.first_name } {props.appointment.technician.last_name}</td>
            <td>{ props.appointment.reason }</td>
            <td>{ status }</td>
            <td><button className='btn btn-danger' onClick={(e) => cancelAppointment(e, props.appointment.id)}>Cancel</button><button onClick={(e) => FinishAppointment(e,props.appointment.id)} className='btn btn-success'>Finish</button></td>
      </tr>
    );
}

export default Appointment;
