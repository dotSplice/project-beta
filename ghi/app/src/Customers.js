import React, {useState, useEffect} from 'react';
import Customer from './Customer';

export default function Customers() {
    const [customers, setCustomers] = useState([]);

    const getCustomers = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setCustomers(data);
        }
    }

    const deleteCustomer = async (id) => {
        const url = `http://localhost:8090/api/customers/${id}`;
        const fetchConfig = {
            method: 'DELETE',
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setCustomers(customers.filter((customer) => customer.id !== id));
        }
    }

    useEffect(() => {
        getCustomers();
    }, []);

    return (
        <div className='container mx-auto mt-5 p-4 bg-light rounded'>
            <h1 className='text-center mb-4 fw-bold'>Customers</h1>
            <table className='table'>
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Phone Number</th>
                        <th>Address</th>
                    </tr>
                </thead>
                <tbody>
                    {customers.map((customer) => {
                        return (
                            <Customer key={customer.id} customer={customer} deleteCustomer={deleteCustomer} />
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
