import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import Technicians from './Technicians';
import TechnicianForm from './TechnicianForm';
import Appointments from './Appointments';
import AppointmentForm from './AppointmentForm';
import Manufacturers from './Manufacturers';
import ManufacturerForm from './ManufacturerForm';
import Models from './Models';
import ModelForm from './ModelForm';
import ServiceHistory from './ServiceHistory';
import Automobiles from './Automobiles';
import AutomobileForm from './AutomobileForm';
import Customers from './Customers';
import CustomerForm from './CustomerForm';
import Sales from './Sales';
import SaleForm from './SaleForm';
import SalespersonForm from './SalespersonForm';
import Salespeople from './Salespeople';
import SalespersonHistory from './SalespersonHistory';


export default function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path = "/technicians" element = {<Technicians />}/>
          <Route path = "/add-technician" element = {<TechnicianForm />} />
          <Route path = "/appointments" element = {<Appointments/>} />
          <Route path = "/add-appointment" element = {<AppointmentForm />} />
          <Route path = "/manufacturers" element = {<Manufacturers />} />
          <Route path = "/add-manufacturer" element = {<ManufacturerForm />} />
          <Route path = "/models" element = {<Models />} />
          <Route path = "/add-model" element = {<ModelForm/>} />
          <Route path = "/service-history" element = {<ServiceHistory/>} />
          <Route path = "/automobiles" element = {<Automobiles/>} />
          <Route path = "/add-automobile" element = {<AutomobileForm/>} />
          <Route path="/salespeople" element={<Salespeople />} />
          <Route path="/add-salesperson" element={<SalespersonForm />} />
          <Route path="/customers" element={<Customers />} />
          <Route path="/add-customer" element={<CustomerForm />} />
          <Route path="/sales" element={<Sales />} />
          <Route path="/add-sale" element={<SaleForm />} />
          <Route path="/sales-history" element={<SalespersonHistory />} />
        </Routes>
      </div>
    </BrowserRouter>
  );
}
