import React, { useState, useEffect } from 'react';
import Automobile from  './Automobile';


function Automobiles(){
    const [automobiles, setAutomobiles] = useState([]);

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/automobiles/';

        const response = await fetch(url);

        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return(
         <table className="table table-striped">
            <thead>
            <tr>
                <th>VIN</th>
                <th>Color</th>
                <th>Year</th>
                <th>Model</th>
                <th>Manufacturer</th>
                <th>Sold</th>
            </tr>
            </thead>
            <tbody>
            {automobiles.map((automobile) => {
                return (
                    <Automobile key={automobile.id} automobile={automobile}/>
                );
            })}
            </tbody>
        </table>
    )
}

export default Automobiles
