import React, { useState, useEffect } from 'react';

export default function SaleForm() {
    const [automobiles, setAutomobiles] = useState([]);
    const [vin, setVin] = useState('');
    const [customers, setCustomers] = useState([]);
    const [customer, setCustomer] = useState('');
    const [salespeople, setSalespeople] = useState([]);
    const [salesperson, setSalesperson] = useState('');
    const [price, setPrice] = useState('');

    const getVin = async () => {
        const url = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setAutomobiles(data.autos);
        }
    }

    const getCustomers = async () => {
        const url = 'http://localhost:8090/api/customers/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setCustomers(data);
        }
    }

    const getSalespeople = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data);
        }
    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {}
        data.automobile = `/api/automobiles/${vin}/`;
        data.salesperson = Number(salesperson);
        data.customer = Number(customer);
        data.price = Number(price);
        const url = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setVin('');
            setSalespeople('');
            setCustomers('');
            setPrice('');
        }
    }

    useEffect(() => {
        getVin();
        getCustomers();
        getSalespeople();
    }, []);

    return (
      <div className='container mx-auto mt-5 p-4 bg-light rounded'>
        <h1 className=' text-center mb-4 fw-bold '>Add a Sale</h1>
          <form onSubmit={handleSubmit}>
            <div className="mb-3">
                <label htmlFor="automobile" className="form-label">Automobile</label>
                  <select className="form-select" id="vin" value={vin} onChange={(event) => setVin(event.target.value)}>
                    <option value="">Choose an Automobile Vin...</option>
                      {automobiles.map((automobile) => (
                        <option key={automobile.vin} value={automobile.vin}>{automobile.vin}</option>
                      ))}
                  </select>
            </div>
            <div className="mb-3">
              <label htmlFor="salesperson" className="form-label">Salesperson</label>
                <select className="form-select" id="salesperson" value={salesperson} onChange={(event) => setSalesperson(event.target.value)}>
                  <option value="">Choose a Salesperson...</option>
                    {salespeople.map((salesperson) => (
                      <option key={salesperson.id} value={salesperson.id}>{salesperson.first_name} {salesperson.last_name}</option>
                    ))}
                </select>
            </div>
              <div className="mb-3">
                <label htmlFor="customer" className="form-label">Customer</label>
                  <select className="form-select" id="customer" value={customer} onChange={(event) => setCustomer(event.target.value)}>
                      <option value="">Choose a Customer...</option>
                        {customers.map((customer) => (
                          <option key={customer.id} value={customer.id}>{customer.first_name} {customer.last_name}</option>
                        ))}
                  </select>
              </div>
              <div className="mb-3">
                  <label htmlFor="price" className="form-label">Price</label>
                  <input type="number" className="form-control" id="price" value={price} onChange={(event) => setPrice(event.target.value)} />
              </div>
              <button type="submit" className="btn btn-primary">Submit</button>
          </form>
      </div>
    )
}
