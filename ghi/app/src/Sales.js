import React, { useState, useEffect } from 'react';
import Sale from './Sale';

export default function Sales() {
    const [sales, setSales] = useState([]);

    const getSales = async () => {
        const url = 'http://localhost:8090/api/sales/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            const dataWithVin = data.map((sale) => {
                return {
                    ...sale,
                    automobile: {
                      ...sale.automobile,
                      vin: sale.automobile.import_href.slice(17, 34)
                    }
                }
            })
            setSales(dataWithVin);
        }
    };

    const deleteSale = async (id) => {
      const url = `http://localhost:8090/api/sales/${id}`;
      const fetchConfig = {
        method: 'DELETE',
      }
      const response = await fetch(url, fetchConfig);
      if (response.ok) {
        setSales(sales.filter(sale => sale.id !== id));
      }
    };

    useEffect(() => {
        getSales();
    }, []);

    return (
        <div className="container">
          <table className="table">
            <thead>
              <tr>
                <th>Salesperson Employee ID</th>
                <th>Salesperson Name</th>
                <th>Customer Name</th>
                <th>VIN</th>
                <th>Price</th>
              </tr>
            </thead>

            <tbody>
              {sales.map((sale) => {
                return (
                  <Sale key={sale.id} sale={sale} deleteSale={deleteSale}/>
                );
              })}
            </tbody>
          </table>
        </div>
    );
}
