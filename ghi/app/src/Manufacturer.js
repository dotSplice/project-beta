import React from 'react';

function Manufacturer(props) {
    return (
        <tr key={props.manufacturer.id}>
            <td>{ props.manufacturer.name }</td>
            <td><button className="btn btn-danger" onClick={() => props.deleteManufacturer(props.manufacturer.id)}>Delete</button></td>
      </tr>
    );
}

export default Manufacturer;
