import React, { useState, useEffect } from 'react';

function AppointmentForm(){
    const [technicians, setTechnicians] = useState([]);
    const [vin, setVin] = useState('');
    const [customer, setCustomer] = useState('');
    const [dateTime, setDateTime] = useState('');
    const [technician, setTechnician] = useState('');
    const [reason, setReason] = useState('');


    const fetchData = async () => {
        const url = 'http://localhost:8080/api/technicians/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setTechnicians(data);
        }

    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {}
        data.vin = vin;
        data.customer = customer;
        data.date_time = dateTime;
        data.technician = Number(technician);
        data.reason = reason;
        data.status = 'pending';
        const url = 'http://localhost:8080/api/appointments/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            // const newAppointment = await response.json();
            setCustomer('');
            setDateTime('');
            setTechnician('');
            setReason('');
            setVin('');
        }

    }

    useEffect(() => {
        fetchData();
    }, []);
    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Book an Appointment</h1>
                    <form onSubmit={handleSubmit} id="book-appointment-form">
                    <div className="form-floating mb-3">
                            <input onChange={(e)=> setVin(e.target.value)} placeholder="Automobile VIN" required type="text" name="vin" id="vin" className="form-control" value={vin}/>
                            <label htmlFor="vin">Automobile VIN</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e)=> setCustomer(e.target.value)} placeholder="Customer" required type="text" name="customer" id="customer" className="form-control" value={customer}/>
                            <label htmlFor="customer">Customer</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e) => setDateTime(e.target.value)} placeholder="Time" required type="datetime-local" name="date_time" id="date_time" className="form-control" value={dateTime}/>
                            <label htmlFor="date_time">Choose an Appointment Time</label>
                        </div>
                        <div className="mb-3">
                                <select onChange={(e)=> setTechnician(e.target.value)} required name="technician" id="technician" className="form-select" value={technician}>
                                <option value="">Choose a Technician</option>
                                {technicians.map(technician => {
                                    return (
                                        <option key={technician.id} value={technician.id}>{technician.first_name} {technician.last_name}</option>
                                    )
                                })}
                                </select>
                            </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e)=> setReason(e.target.value)} placeholder="Reason" required type="text" name="reason" id="reason" className="form-control" value={reason}/>
                            <label htmlFor="reason">Reason</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Book Appointment</button>
                    </form>
                </div>
            </div>
        </div>
    )

}

export default AppointmentForm
