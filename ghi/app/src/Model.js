import React from 'react';

function Model(props){
    return(
        <tr key={props.model.id}>
            <td>{ props.model.name }</td>
            <td>{ props.model.manufacturer.name }</td>
            <td><img style={{ width: '300px', height: '200px' }}src={ props.model.picture_url } /></td>
            <td><button className="btn btn-danger" onClick={() => props.deleteModel(props.model.id)}>Delete</button></td>
      </tr>
    )
}

export default Model
