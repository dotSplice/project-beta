import React from 'react';

function Automobile(props){
    return(
        <tr key={props.automobile.id}>
            <td>{ props.automobile.vin }</td>
            <td>{ props.automobile.color }</td>
            <td>{ props.automobile.year }</td>
            <td>{ props.automobile.model.name}</td>
            <td>{ props.automobile.model.manufacturer.name }</td>
            <td>{ props.automobile.sold  ? 'Yes' : 'No' }</td>
      </tr>
    )
}

export default Automobile
