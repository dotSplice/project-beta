import React, { useState, useEffect } from 'react';

function AutomobileForm() {
    const [models, setModels] = React.useState([]);
    const [ color, setColor ] = React.useState('');
    const [ year, setYear ] = React.useState('');
    const [ vin, setVin ] = React.useState('');
    const [ model, setModel ] = React.useState('');

    const fetchData = async () => {
        const url = 'http://localhost:8100/api/models/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setModels(data.models);
        }

    }

    const handleSubmit = async (e) => {
        e.preventDefault();
        const data = {}
        data.color = color;
        data.year = Number(year);
        data.vin = vin;
        data.model_id = Number(model);
        const url = 'http://localhost:8100/api/automobiles/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            // const newModel = await response.json();
            setColor('');
            setYear('');
            setVin('');
            setModel('');
        }
    }

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add an automobile to inventory</h1>
                    <form onSubmit={handleSubmit} id="add-automobile-form">
                        <div className="form-floating mb-3">
                            <input onChange={(e)=> setColor(e.target.value)} placeholder="Color" required type="text" name="color" id="color" className="form-control" value={color}/>
                            <label htmlFor="color">Color</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e) => setYear(e.target.value)} placeholder="Year" required type="text" name="year" id="year" className="form-control" value={year}/>
                            <label htmlFor="year">Year</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange={(e)=> setVin(e.target.value)} placeholder="Automobile VIN" required type="text" name="vin" id="vin" className="form-control" value={vin}/>
                            <label htmlFor="vin">Automobile VIN</label>
                        </div>
                        <div className="mb-3">
                                <select onChange={(e)=> setModel(e.target.value)} required name="model" id="model" className="form-select" value={model}>
                                <option value="">Choose a Model</option>
                                    {models.map(model => {
                                        return (
                                            <option key={model.id} value={model.id}>{model.manufacturer.name} - {model.name}</option>
                                        )
                                    })}
                                </select>
                            </div>
                        <button type="submit" className="btn btn-primary">Add Automobile</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default AutomobileForm
