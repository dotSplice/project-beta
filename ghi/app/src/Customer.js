import React from "react";


export default function Customer(props) {
    return (
        <tr key={props.customer.id}>
            <td>{props.customer.first_name}</td>
            <td>{props.customer.last_name}</td>
            <td>{props.customer.phone_number}</td>
            <td>{props.customer.address}</td>
            <td><button className="btn btn-danger" onClick={() => props.deleteCustomer(props.customer.id)}>Delete</button></td>
        </tr>
    )
}
