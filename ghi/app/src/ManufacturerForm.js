import React, { useState } from 'react';

function ManufacturerForm() {
    const [manufacturer, setManufacturer] = useState('');

    const handleSubmit = async(e) => {
        e.preventDefault();
        const data = {};
        data.name = manufacturer;
        const url = 'http://localhost:8100/api/manufacturers/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json'
            }
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            const newManufacturer = await response.json();
            setManufacturer('');

        }

    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
            <div className="shadow p-4 mt-4">
                <h1>Add a Manufacturer</h1>
                    <form onSubmit={handleSubmit} id="create-technician-form">
                        <div className="form-floating mb-3">
                            <input onChange={(e)=> setManufacturer(e.target.value)} placeholder="Manufacturer" required type="text" name="name" id="name" className="form-control" value={manufacturer}/>
                            <label htmlFor="name">Manufacturer</label>
                        </div>
                        <button type="submit" className="btn btn-primary">Add Manufacturer</button>
                    </form>
            </div>
        </div>
        </div>
    )
}

export default ManufacturerForm
