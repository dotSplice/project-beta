import React, { useState, useEffect } from 'react';
import Salesperson from './Salesperson';

export default function Salespeople() {
    const [salespeople, setSalespeople] = useState([]);

    const getSalespeople = async () => {
        const url = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setSalespeople(data);
        }
    };

    const deleteSalesperson = async (id) => {
        const url = `http://localhost:8090/api/salespeople/${id}`;
        const fetchConfig = {
            method: 'DELETE',
        }
        const response = await fetch(url, fetchConfig);
        if (response.ok) {
            setSalespeople(salespeople.filter(salesperson => salesperson.id !== id));
        }
    };

    useEffect(() => {
        getSalespeople();
    }, []);

    return (
        <div className='container mx-auto mt-5 p-4 bg-light rounded'>
            <h1 className='text-center mb-4 fw-bold'>Salespeople</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th scope="col">Employee ID</th>
                        <th scope="col">First Name</th>
                        <th scope="col">Last Name</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople.map((salesperson) => {
                        return (
                            <Salesperson key={salesperson.id} salesperson={salesperson} deleteSalesperson={deleteSalesperson}/>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}
