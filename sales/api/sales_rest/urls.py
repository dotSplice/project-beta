from django.urls import path
from .views import (
    sales,
    delete_sale,
    customers,
    delete_customer,
    salespeople,
    delete_salesperson
)


urlpatterns = [
    path("sales/", sales, name="sales"),
    path("sales/<int:id>", delete_sale, name="delete_sale"),
    path("customers/", customers, name="customers"),
    path(
        "customers/<int:id>",
        delete_customer,
        name="delete_customer"
        ),
    path("salespeople/", salespeople, name="salespeople"),
    path(
        "salespeople/<int:id>",
        delete_salesperson,
        name="delete_salesperson"
        )
]
