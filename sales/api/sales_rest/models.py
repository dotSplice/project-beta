from django.db import models


class AutomobileVO(models.Model):
    import_href = models.URLField(max_length=500, unique=True)
    sold = models.BooleanField(default=False)
    vin = models.CharField(max_length=17)


class Salesperson(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    employee_id = models.IntegerField(unique=True)

    def __str__(self):
        return self.employee_id


class Customer(models.Model):
    first_name = models.CharField(max_length=20)
    last_name = models.CharField(max_length=20)
    address = models.CharField(max_length=50)
    phone_number = models.CharField(max_length=10, unique=True)

    def __str__(self):
        return self.first_name


class Sale(models.Model):
    automobile = models.ForeignKey(
        AutomobileVO, related_name="sales", on_delete=models.CASCADE
        )
    salesperson = models.ForeignKey(
        Salesperson, related_name="sales", on_delete=models.CASCADE
    )
    customer = models.ForeignKey(
        Customer, related_name="sales", on_delete=models.CASCADE
    )
    price = models.IntegerField()

    def __str__(self):
        return self.price
