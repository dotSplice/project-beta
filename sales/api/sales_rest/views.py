from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json
from .encoders import (
    SaleEncoder,
    CustomerEncoder,
    SalespersonEncoder
)
from .models import (
    Customer,
    Sale,
    Salesperson,
    AutomobileVO
)


@require_http_methods(["GET", "POST"])
def sales(request):
    if request.method == "GET":
        sales = Sale.objects.all()
        return JsonResponse(
            sales, encoder=SaleEncoder, safe=False
            )
    elif request.method == "POST":
        data = json.loads(request.body)
        try:
            href = data["automobile"]
            automobile = AutomobileVO.objects.get(import_href=href)
        except AutomobileVO.DoesNotExist:
            return JsonResponse(
                {"message": "Could not find the automobile"}, status=404
            )
        try:
            customer_id = data["customer"]
            customer = Customer.objects.get(id=customer_id)
        except Customer.DoesNotExist:
            return JsonResponse(
                {"message": "Could not find the customer"}, status=404
            )
        try:
            salesperson_id = data["salesperson"]
            salesperson = Salesperson.objects.get(id=salesperson_id)
        except Salesperson.DoesNotExist:
            return JsonResponse(
                {"message": "Could not find the salesperson"}, status=404
            )
        data["automobile"] = automobile
        data["customer"] = customer
        data["salesperson"] = salesperson
        new_sale = Sale.objects.create(**data)
        return JsonResponse(new_sale, encoder=SaleEncoder, safe=False)


@require_http_methods(["DELETE"])
def delete_sale(request, id):
    try:
        sale = Sale.objects.get(id=id)
        sale.delete()
        return JsonResponse(
            sale,
            encoder=SaleEncoder,
            safe=False
            )
    except Sale.DoesNotExist:
        return JsonResponse(
            {"message": "Could not find the sale"}, status=404
        )


@require_http_methods(["GET", "POST"])
def customers(request):
    if request.method == "GET":
        customers = Customer.objects.all()
        return JsonResponse(
            customers,
            encoder=CustomerEncoder,
            safe=False
            )
    else:  # POST
        content = json.loads(request.body)
        new_customer = Customer.objects.create(**content)
        return JsonResponse(
            new_customer,
            encoder=CustomerEncoder,
            safe=False
            )


@require_http_methods(["DELETE"])
def delete_customer(request, id):
    try:
        customer = Customer.objects.get(id=id)
        customer.delete()
        return JsonResponse(
            customer,
            encoder=CustomerEncoder,
            safe=False
            )
    except Customer.DoesNotExist:
        response = JsonResponse(
            {"message": "Could not find the customer"}
        )
        response.status_code = 404
        return response


@require_http_methods(["GET", "POST"])
def salespeople(request):
    if request.method == "GET":
        try:
            salespeople = Salesperson.objects.all()
            return JsonResponse(
                salespeople,
                encoder=SalespersonEncoder,
                safe=False
                )
        except Salesperson.DoesNotExist:
            response = JsonResponse(
                {"message": "Could not find the salespeople"}
            )
            response.status_code = 400
            return response

    else:
        content = json.loads(request.body)
        salespeople = Salesperson.objects.create(**content)
        return JsonResponse(
            salespeople,
            encoder=SalespersonEncoder,
            safe=False
            )


@require_http_methods(["DELETE"])
def delete_salesperson(request, id):
    if request.method == "DELETE":
        try:
            salesperson = Salesperson.objects.get(id=id)
            salesperson.delete()
            return JsonResponse(
                salesperson,
                encoder=SalespersonEncoder,
                safe=False
                )
        except Salesperson.DoesNotExist:
            response = JsonResponse(
                {"message": "Could not find the salesperson"}
            )
            response.status_code = 404
            return response
