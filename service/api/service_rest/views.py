from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from django.views.decorators.http import require_http_methods
from .models import Technician, AutomobileVO, Appointment
from common.json import ModelEncoder
import json

# Create your views here.

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = ["id", "first_name", "last_name", "employee_id"]
    def get_extra_data(self, o):
        return {"href": o.get_api_url() if hasattr(o, "get_api_url") else None}

class AppointmentListEncoder(ModelEncoder):
    model = Appointment
    properties = ["id", "date_time", "reason", "status", "customer", "vin", "vip", "technician"]

    encoders = {
        "technician": TechnicianListEncoder()
    }

@require_http_methods(["GET", "POST"])
def api_list_technicians(request):
    if request.method == 'GET':
        try:
            technician = Technician.objects.all()
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

    elif request.method == 'POST':
        try:
            content = json.loads(request.body)
            technician = Technician.objects.create(**content)
            return JsonResponse(technician, encoder=TechnicianListEncoder, safe=False)
        except:
            response = JsonResponse(
                {"message": "Could not create the technician"}
            )
            response.status_code = 400
            return response

@require_http_methods(["GET", "DELETE"])
def api_get_technician_details(request, id):
    if request.method == 'GET':
        try:
            technician = Technician.objects.get(id=id)
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == 'DELETE':
        try:
            technician = Technician.objects.get(id=id)
            technician.delete()
            return JsonResponse(
                technician,
                encoder=TechnicianListEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response



@require_http_methods(["GET", "POST"])
def api_list_appointments(request):
    if request.method == 'GET':
        try:
            appointment = Appointment.objects.all()
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response


    elif request.method == 'POST':
        content = json.loads(request.body)
        technician_id = content['technician']
        try:
            technician = Technician.objects.get(id=technician_id)
            vip = AutomobileVO.objects.filter(vin=content['vin']).exists()
            print("VIP", vip)
        except Technician.DoesNotExist:
            return HttpResponse(f"Technician with ID {technician_id} does not exist.", status=400)
        print(vip)
        appointment = Appointment.objects.create(
            date_time=content['date_time'],
            customer=content['customer'],
            vip=vip,
            reason=content['reason'],
            status=content['status'],
            technician=technician,
            vin=content['vin']
        )
        return JsonResponse(appointment, encoder=AppointmentListEncoder, safe=False)

require_http_methods(["GET", "DELETE"])
def api_get_appointment_details(request, id):
    if request.method == 'GET':
        try:
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
    elif request.method == 'DELETE':
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.delete()
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

require_http_methods(["PUT"])
def api_cancel_appointment(request, id):
    if request.method == 'PUT':
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status = "canceled"
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response

def api_finish_appointment(request, id):
    if request.method == 'PUT':
        try:
            appointment = Appointment.objects.get(id=id)
            appointment.status = "finished"
            appointment.save()
            return JsonResponse(
                appointment,
                encoder=AppointmentListEncoder,
                safe=False,
            )
        except Appointment.DoesNotExist:
            response = JsonResponse({"message": "Does not exist"})
            response.status_code = 404
            return response
