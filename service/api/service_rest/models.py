from django.db import models
from django.urls import reverse


# Create your models here.
class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=1000, unique=True)
    vin = models.CharField(max_length=17, unique=True)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=100)
    last_name = models.CharField(max_length=100)
    employee_id = models.CharField(max_length=100, unique=True)

    def __str__(self):
        return self.employee_id

    def get_api_url(self):
        return reverse("api_get_technician_details", kwargs={"pk": self.id})


class Appointment(models.Model):
    date_time = models.DateTimeField()
    reason = models.CharField(max_length=300)
    status = models.CharField(max_length=100)
    customer = models.CharField(max_length=100)
    vip = models.BooleanField(default=False)
    vin = models.CharField(max_length=17)
    technician = models.ForeignKey(
        Technician, related_name="appointments", on_delete=models.CASCADE
    )
