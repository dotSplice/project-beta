from django.urls import path, include
from .views import api_list_technicians, api_list_appointments, api_get_technician_details, api_get_appointment_details, api_cancel_appointment, api_finish_appointment

urlpatterns = [
    path('technicians/', api_list_technicians, name='api_list_technicians'),
    path('technicians/<int:pk>/', api_get_technician_details, name='api_get_technician_details'),
    path('appointments/', api_list_appointments, name='api_list_appointments'),
    path('appointments/<int:id>/', api_get_appointment_details, name='api_get_appointment_details'),
    path('appointments/<int:id>/cancel', api_cancel_appointment, name='api_cancel_appointment'),
    path('appointments/<int:id>/finished', api_finish_appointment, name='api_finish_appointment'),

]
