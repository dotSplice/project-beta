# CarCar

Team:

* Person 1 - Which microservice?
* brooks - sales
* Tyler - Service microservice
* Person 2 - Which microservice?

## Design

## Service microservice

The service microservice is integrated with the inventory api by making use of polling. the poller function updates or creates the automobile value object in our microservice models. the other models are for technicians and appointments. the AutoVO is mainly used to display whether or not the customer is a VIP customer by checking to see if the vin exists in the VO upon the creation of a new service appointment. other than that, it is not used, so it could/should contain less data, i.e doesn't need to contain (currently) whether the auto has been sold. our microservice service backend is integrated with our front end via calls to our api endpoints to fetch data from our db and display it for the user.

## Sales microservice

The sales microservice integrates its Sale, Salesperson, Customer, and Vehicle models with encoders and react components. The vehiche model in the car api is acessed as a value object snd is acessed also by its href. We have defined a single instance of each of the models to be reused in corresponding javascript files(add-new-form/list-or-delete-form). The salesperson has has history component as well filtered by salesperson, and reveales sold vehicles only.
